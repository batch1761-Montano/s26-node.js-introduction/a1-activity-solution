/*Activity:
	Create a new http server in activity.js using the http module of node.
	This http server should run on port 5000.

	Create routes for the following endpoints:

"/"	
	-write the appropriate headers for our response with writeHead()
		-status 200, Content-Type: text/plain
	-end the response with .end() and send the following message:
	"Welcome to B176 Booking System."

"/courses"
	-write the appropriate headers for our response with writeHead()
		-status 200, Content-Type: text/plain
	-end the response with .end() and send the following message:
	"Welcome to the Courses page. View our Courses."

"/profile"	
	-write the appropriate headers for our response with writeHead()
		-status 200, Content-Type: text/plain
	-end the response with .end() and send the following message:
	"Welcome to your profile. View your details"

and an else which will be a route for the undefined and undesignated endpoint 
-write the appropriate headers for our response with writeHead()
		-status 404, Content-Type: text/plain
	-end the response with .end() and send the following message:	
	"Resource not found."



*/

let http = require("http");

http.createServer(function(request,response){


	if(request.url === "/"){
		
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Welcome to B176 Booking System.");

	} else if (request.url === "/courses"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Welcome to the Courses Page!");

	} else if (request.url === "/profile"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Welcome to your Profile. View your details.")

	} else {
		
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end("Resource cannot be found.");
	}

}).listen(5000);

console.log("Server is running on localhost:5000");
